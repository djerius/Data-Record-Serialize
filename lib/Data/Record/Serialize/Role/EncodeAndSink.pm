package Data::Record::Serialize::Role::EncodeAndSink;

# ABSTRACT: Both an Encode and Sink. handle unwanted/unused required routines

use v5.12;
use strict;
use warnings;

our $VERSION = '2.02';

use Data::Record::Serialize::Error { errors => [qw( internal  )] }, -all;

use Moo::Role;

use namespace::clean;

## no critic ( Subroutines::ProhibitBuiltinHomonyms )
## no critic(BuiltinFunctions::ProhibitComplexMappings)

# These are not used for a combined encoder and sink; if
# they are called it's an internal error, so create versions
# to catch them.

sub say;
sub print;
sub encode;

( *say, *print, *encode ) = map {
    my $stub = $_;
    sub { error( 'internal', "internal error: stub method <$stub> invoked" ) }
} qw( say print encode );

with 'Data::Record::Serialize::Role::Sink';
with 'Data::Record::Serialize::Role::Encode';

1;

# COPYRIGHT

__END__

=for Pod::Coverage
say
print
encode
close


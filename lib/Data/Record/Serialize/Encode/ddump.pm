package Data::Record::Serialize::Encode::ddump;

# ABSTRACT:  encoded a record using Data::Dumper

use v5.12;
use Moo::Role;

our $VERSION = '2.02';

use Scalar::Util;
use Data::Dumper;
use Data::Record::Serialize::Error { errors => ['::parameter'] }, -all;
use namespace::clean;

sub _needs_eol { 1 }

=attr ddump

The L<Data::Dumper> object. It will be constructed if not provided to
the constructor.

=cut

has ddump => (
    is      => 'lazy',
    isa     => sub { Scalar::Util::blessed $_[0] && $_[0]->isa( 'Data::Dumper' ) },
    builder => sub {
        Data::Dumper->new( [] );
    },
);

=attr dd_config

Configuration data for the L<Data::Dumper> object stored in L</ddump>.
Hash keys are the names of L<Data::Dumper> configuration variables,
without the preceding C<Data::Dumper::> prefix.  Be careful to ensure
that the resultant output is a (comma separated) list of structures
which can be C<eval>'ed.

B<Terse> and B<Trailingcomma> are always set.

=cut

has dd_config => (
    is  => 'ro',
    isa => sub {
        ref $_[0] eq 'HASH'
          or error( '::parameter' => q{'config' parameter  must be a hashref'} );
    },
    default => sub { {} },
);

=for Pod::Coverage
  encode

=cut

sub encode {
    my $self = shift;
    $self->ddump->Values( \@_ )->Dump . q{,};
}

around BUILD => sub {
    my ( $orig, $self ) = ( shift, shift );

    $orig->( $self, @_ );

    my $ddump  = $self->ddump;
    my %config = (
        %{ $self->dd_config },
        Terse         => 1,
        Trailingcomma => 1,
    );

    for my $mth ( keys %config ) {
        my $code = $ddump->can( $mth )
          or error( '::parameter', "$mth is not a Data::Dumper configuration variable" );
        $code->( $ddump, $config{$mth} );
    }
    return;
};

=class_method new

This role adds two named arguments to the constructor, L</ddump> and
L</config>, which mirror the added object attributes.

=cut

with 'Data::Record::Serialize::Role::Encode';

1;

# COPYRIGHT

__END__

=for stopwords
Trailingcomma

=head1 SYNOPSIS

    use Data::Record::Serialize;

    my $s = Data::Record::Serialize->new( encode => 'ddump', ... );

    $s->send( \%record );

=head1 DESCRIPTION

B<Data::Record::Serialize::Encode::ddump> encodes a record using
L<Data::Dumper>.  The resultant encoding may be decoded via

  @data = eval $buf;

It performs the L<Data::Record::Serialize::Role::Encode> role.

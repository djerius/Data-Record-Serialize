package Data::Record::Serialize::Encode::array;

# ABSTRACT: encoded a record as /rdb

use v5.12;
use Moo::Role;

our $VERSION = '2.02';

use namespace::clean;

=for Pod::Coverage
  setup

=cut

sub setup {
    my $self = shift;
    $self->say( $self->output_fields );
}

=for Pod::Coverage
 encode

=cut

sub encode {
    my $self = shift;

    ## no critic (TestingAndDebugging::ProhibitNoWarnings)
    no warnings 'uninitialized';
    return [ @{ $_[0] }{ @{ $self->output_fields } } ];
}

with 'Data::Record::Serialize::Role::Encode';

1;

# COPYRIGHT

__END__

=head1 SYNOPSIS

    use Data::Record::Serialize;

    my $s = Data::Record::Serialize->new( encode => 'array', ... );

    $s->send( \%record );

=head1 DESCRIPTION

B<Data::Record::Serialize::Encode::array> encodes a record as
a Perl arrayref. The first array output will contain the field names.

For example,

  my @output;
  $s = Data::Record::Serialize->new(
      encode  => 'array',
      sink    => 'array',
      output  => \@output,
      fields  => [ 'integer', 'number', 'string1', 'string2', 'bool' ],
  );

  $s->send( {
      integer => 1,
      number  => 2.2,
      string1 => 'string',
      string2 => 'nyuck nyuck',
  } );

results in

  @output = (
    ["integer", "number", "string1", "string2", "bool"],
    [1, 2.2, "string", "nyuck nyuck", undef],
  )


It performs the L<Data::Record::Serialize::Role::Encode> role.


=head1 INTERFACE

There are no additional attributes which may be passed to
L<Data::Record::Serialize-E<gt>new>|Data::Record::Serialize/new>.

package Data::Record::Serialize::Encode::rdb;

# ABSTRACT: encoded a record as /rdb

use v5.12;
use Moo::Role;

our $VERSION = '2.02';

use namespace::clean;

sub _needs_eol { 1 }
sub _map_types { { N => 'N', I => 'N', S => 'S' } }


=for Pod::Coverage
  setup

=cut

sub setup {
    my $self = shift;

    $self->say( join( "\t", @{ $self->output_fields } ) );
    $self->say( join( "\t", @{ $self->output_types }{ @{ $self->output_fields } } ) );
}

=for Pod::Coverage
 encode

=cut

sub encode {
    my $self = shift;

    ## no critic (TestingAndDebugging::ProhibitNoWarnings)
    no warnings 'uninitialized';
    join( "\t", @{ $_[0] }{ @{ $self->output_fields } } );
}

with 'Data::Record::Serialize::Role::Encode';

1;

# COPYRIGHT

__END__

=head1 SYNOPSIS

    use Data::Record::Serialize;

    my $s = Data::Record::Serialize->new( encode => 'rdb', ... );

    $s->send( \%record );

=head1 DESCRIPTION

B<Data::Record::Serialize::Encode::rdb> encodes a record as
L<RDB|http://compbio.soe.ucsc.edu/rdb>.

It performs the L<Data::Record::Serialize::Role::Encode> role.


=head1 INTERFACE

There are no additional attributes which may be passed to
L<Data::Record::Serialize-E<gt>new>|Data::Record::Serialize/new>.

package Data::Record::Serialize::Encode::null;

# ABSTRACT: infinite bitbucket

use v5.12;
use Moo::Role;

our $VERSION = '2.02';

use namespace::clean;

=for Pod::Coverage
 encode
 send
 print
 say
 close

=cut

## no critic ( Subroutines::ProhibitBuiltinHomonyms )
## no critic( NamingConventions::ProhibitAmbiguousNames )

sub encode { }
sub send   { }
sub print  { }
sub say    { }
sub close  { }

with 'Data::Record::Serialize::Role::Encode';
with 'Data::Record::Serialize::Role::Sink';

1;

# COPYRIGHT

__END__

=for stopwords
bitbucket

=head1 SYNOPSIS

    use Data::Record::Serialize;

    my $s = Data::Record::Serialize->new( encode => 'null', ... );

    $s->send( \%record );

=head1 DESCRIPTION

B<Data::Record::Serialize::Encode::null> is both an encoder and a sink.
All records sent using it will disappear.

It performs both the L<Data::Record::Serialize::Role::Encode> and
L<Data::Record::Serialize::Role::Sink> roles.

=head1 INTERFACE

There are no additional attributes which may be passed to
L<< Data::Record::Serialize::new|Data::Record::Serialize/new >>.
